.cpu cortex-m4
.syntax unified
.thumb
.text
.align	2


/* float gasmAbsf(float num); */
 .global	gasmAbsf
.thumb_func
	gasmAbsf:
	vabs.f32 s0,s0
	bx lr
	
/* int32_t gasmAbs32(int32_t num); */
.global		gasmAbs32
.thumb_func
	gasmAbs32:

	vpush {s0}

	vmov s0,r0
	vcvt.f32.s32 s0,s0
	vabs.f32 s0,s0 // lol, I liked this better than any example I found.
	vcvt.s32.f32 s0,s0
	vmov r0,s0
	
	vpop {s0}
	bx lr
	
/* int32_t gasmTest1(void); */
.global		gasmTest1
.thumb_func
	gasmTest1:
	push {lr}
//	mov lr,gasmTest1_return
	bl micros

// gasmTest1_return:
	pop {lr}
	bx lr