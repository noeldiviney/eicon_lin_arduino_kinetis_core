// propose object to accept 4 inputs and offer 3 outputs
// each of the three outputs capable of selecting one input at a time

#ifndef muxer_h_
#define muxer_h_

#include <EiconPreampStructs.h>

#include "Arduino.h"
#include "AudioStream48.h"


class AudioMuxer43 : public AudioStream
{
public:
    AudioMuxer43(input_control_t* ipc, packed_coefficients_t* pct) : AudioStream(4, inputQueueArray) {
		config=&ipc->channel[0];
		ch0f=&pct->ch0[0].b0;
		ch1f=&pct->ch1[0].b0;
		ch2f=&pct->ch2[0].b0;
	}
    AudioMuxer43(void) : AudioStream(4, inputQueueArray) { }
    virtual void update(void);
	void mux(uint8_t n, uint8_t s) {
		if(s>3)
		{
			control_words[n]&=~(31<<16);
			control_words[n]|=(s<<16);
		} else {
			control_words[n]&=~(3<<16);
			control_words[n]|=(s<<16);
		}
	}
	
private:
	audio_block_t *inputQueueArray[4];
	uint8_t *config=NULL;
	float *ch0f=NULL;
	float *ch1f=NULL;
	float *ch2f=NULL;
};

#endif
