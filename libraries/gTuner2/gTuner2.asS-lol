
.cpu cortex-m4
.syntax unified
.thumb

itemp	.req	r0
pconst	.req	r1
pdata	.req	r2
bapo0	.req	r3
bapo1	.req	r4
offs0	.req	r5
offs1	.req	r6
item0	.req	r7
item1	.req	r8
item2	.req 	r9
usec	.req	r10
x4		.req	r11

ftemp	.req	s0
fmp0	.req	s1
fmp1	.req	s2
fmp2	.req	s3
fmp3	.req	s4
ftt0	.req	s5
ftt1	.req	s6
ftt2	.req	s7
ftt3	.req	s8
fac0	.req	s9
fac1	.req	s10
fdiv	.req	s11
scope	.req	s12

.data
.align 2

// gasmTuner_mark_this:

//	[pdata,-160]		ISR falling samples
	.float	12000522,12000550,12000182,12000210
	.float	12000320,12000308,12000375,12000335
	.float	12000615,12000140,12000190,12000098
	.float	12000212,12000212,12000480,12000575
//	[pdata,-96]		ISR rising samples
	.float	12000198,12000140,12000300,12000322
	.float	12000385,12000402,12000078,12000040
	.float	12000082,12000105,12000312,12000288
	.float	12000422,12000462,12000562,12715768
//	[pdata,-32]		ISR sample pointers
	.word	0,0
//	[pdata,-24]		ISR previous timer counts
	.word	0,0,0,0
//	[pdata,-8]		ISR current step
	.word	0
//	[pdata,-4]		SPARE
	.word	0
	
gasmTuner_data:

//	[pdata,0]		current answer.
	.word	0x70C
//	[pdata,4]		current frequency
	.float	0
//	[pdata,8]		current difference
	.float	0
//	[pdata,12]		current octave
	.word	0
//	[pdata,16]		last hit timer counter
	.word	0
//	[pdata,20]		resume branch location
	.word	0
//	[pdata,24]		return branch location
	.word	0
//	[pdata,28]		trackers for ISR sample pointers
	.word	0,0
//	[pdata,36]		tuning_scope (initial)
	.float	0.0005
//	[pdata,40]		maxMatch
	.word	32
//	[pdata,44]		minMatch
	.word	24
//	[pdata,48]		note tolerance
	.float	0.1
//	[pdata,52]		prev_averages_pointer
	.word	0
//	[pdata,56]		prev_averages
	.float	0,0
//	[pdata,64]		scratch0
	.word	0,0,0,0
	.word	0,0,0,0
	.word	0,0,0,0
	.word	0,0,0
//	[pdata,124]		suggestion offset
	.word	0
//	[pdata,128]		nTols
	.float	0.001571,0.001665,0.001764,0.001868
	.float	0.00198,0.002097,0.002222,0.002354
	.float	0.002494,0.002642,0.002799,0.002966
//	[pdata,176]		suggestions
	.word	0,0,0,0 // 
//	[pdata,192]		falling samples (copy)
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
//	[pdata,256]		rising samples (copy)
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
//	[pdata,320]		accums
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
//	[pdata,384]		mid-accums
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000

//	[pdata,448]		sub_accums - first entry is running average.
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000
	.float	12000000,12000000,12000000,12000000

.text
.align 2

/* void gasmTunerISR(void) */
.global gasmTunerISR
.thumb_func

gasmTunerISR:
	
	push {itemp-offs1}
	vpush {ftemp-fmp0}

	ldr itemp,counter_location
	ldr itemp,[itemp] // itemp = counter value
	
	ldr pconst,gasmTuner_const_ptr
	ldr pdata,[pconst]

	ldr bapo0,[pdata,-8] // bapo0=step;
	add bapo1,bapo0,1
	and bapo1,3
	str bapo1,[pdata,-8] // next step stored, now to deal with this one.
	
	add bapo1,pdata,-24 // base of prev_count pointers
	mov offs0,4
	mul offs0,offs0,bapo0 // make offset to prev_count
	ldr offs1,[bapo1,offs0] // get prev counter into offs1
	str itemp,[bapo1,offs0] // place current counter
	
	sub itemp,itemp,offs1 // get interval count here
	vmov ftemp,itemp
	vcvt.f32.u32 ftemp,ftemp // now interval count is a nice float
	vmov.f32 fmp0,2.0
	vdiv.f32 ftemp,ftemp,fmp0 // and a correctly scaled one at that
	
	and offs0,bapo0,1 // and out bit 1, we have a multiplier.
	and bapo0,2 // and out bit 0, we will be left with a 0 or a 2
	add bapo0,bapo0,bapo0 // now we have 0 or 4
	
	add pconst,pdata,-32 // point at sample offset figures
	add pdata,pdata,-160 // point at isr falling samples
	
	mov bapo1,16
	mla pdata,bapo0,bapo1,pdata // pdata+=bapo0*bapo1; shift pdata to gasmTuner_rising if bapo0 is 4
	mov bapo1,32
	mla pdata,offs0,bapo1,pdata // shift halfway through this group if bit 0 was set - samples are being collected in a rotisserie fashion reflecting how they are made.
	
	ldr bapo1,[pconst,bapo0] // grab current sample sub pointer
	cbnz offs0,gasmTunerISR_skip0
	
	sub bapo1,bapo1,1
	and bapo1,7
	str bapo1,[pconst,bapo0] // store changed sample pointer for partner
gasmTunerISR_skip0:	
	mov itemp,4
	mla pdata,bapo1,itemp,pdata // get actual offset to place this new sample, and finally;
	
	vstr ftemp, [pdata] // store the sample appropriately.

	vpop {ftemp-fmp0}
	pop {itemp-offs1}
	bx lr // 1 cycle




	
counter_location:
//	[pconst,-20]
	.word	0xE0001004
//	[pconst,-16]		octava
	.float	31.7854509955
//	[pconst,-12]		timer_count_per_tenth*2_u32
	.word	23999980
//	[pconst,-8]		timer_count_per_second_f32
	.float	120000000
//	[pconst,-4]		default answer
	.word	0x70C

gasmTuner_const:
//	[pconst,0]		pointers, starting with local data
	.word	gasmTuner_data,gasmTuner_start3,gasmTuner_finalAverage,gasmTuner_pageant // 0,4,8,12
	.word	gasmTuner_resulter,gasmTuner_subAccums,gasmTuner_sub2,gasmTuner_sub_result // 16,20,24,28
	.word	gasmTuner_lastChance,gasmTuner_sub5,0,0 // 32,36,40,44
//	[pconst,48]		Note frequency data
	.float	16.3515978312874,17.3239144360545,18.354047994838,19.4454364826301
	.float	20.6017223070544,21.8267644645627,23.1246514194771,24.4997147488593
	.float	25.9565435987466,27.5,29.1352350948806,30.8677063285078
//	[pconst,	96]		Note difference ratios
	.float	1.41421356237309,1.33483985417003,1.25992104989487,1.18920711500272
	.float	1.12246204830937,1.0594630943593,1,0.943874312681693
	.float	0.890898718140336,0.840896415253713,0.793700525984099,0.749153538438338
//	[pconst,144]		Note Names
	.byte	'C',0,0,0,'C','#',0,0,'D',0,0,0,'D','#',0,0
	.byte	'E',0,0,0,'F',0,0,0,'F','#',0,0,'G',0,0,0
	.byte	'G','#',0,0,'A',0,0,0,'A','#',0,0,'B',0,0,0
	.byte	'-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	


gasmTuner_const_ptr:
	.word	gasmTuner_const
	


	
/* uint32_t* gasmTuner_getBit(int32_t offset); */
.global gasmTuner_getBit
.thumb_func

gasmTuner_getBit:

	@itemp: offset

	push {pconst}
	ldr pconst,gasmTuner_const // cut to chase, point at gasmTuner_data first hit.
	add itemp,pconst
	pop {pconst}

	@itemp: bits+offset
	
	bx lr

	
/* uint32_t* gasmTuner_getPtr(int32_t offset); */
.global gasmTuner_getPtr
.thumb_func

gasmTuner_getPtr:

	@itemp: offset

	push {pconst}
	ldr pconst,gasmTuner_const_ptr
	add itemp,pconst
	pop {pconst}
	@itemp: ptrs+offset
	
	bx lr
	
	
/* uint8_t* gasmTuner_noteName(uint8_t noteNum); */
.global gasmTuner_noteName
.thumb_func

gasmTuner_noteName:

	@itemp: noteNum
	
	push {pconst-pdata} // supposedly I can trash these but they can eat me, I am very picky about what I trash.
	
	ldr pconst,gasmTuner_const_ptr // point at the pointer list
	add pconst,pconst,144 // point at names
	mov pdata,4 // prepare multipland
	and itemp,15 // reduce noteName if foolishness has occurred.
	mla itemp,itemp,pdata,pconst // point at the required note name.
	
	@itemp: noteName :)
	
	pop {pconst-pdata}
	
	bx lr

	
/* void gasmTunerTolerance(float newTolerance) */
.global gasmTunerTolerance
gasmTunerTolerance:

	// rewrite the nTol entries based on this value.
	@ftemp: newTolerance
	
	push {pconst-offs0}
	vpush {fmp0}
	
	ldr pdata,gasmTuner_const_ptr
	ldr pconst,[pdata]
	add pdata,pdata,96 // pointing at difference ratios
	add pconst,pconst,128 // pointing at _data.ntols
	mov bapo0,12 // set up a counter to use also as sub-pointer
	mov bapo1,4 // set up a multiplier
	add pconst,44 // actually, I want to reverse the order for these.
gasmTunerTolLoop:
	sub bapo0,1 // decrement bapo0
	mla offs0,bapo0,bapo1,pdata // point at this ratio
	vldr fmp0,[offs0] // grab the ratio
	vmul.f32 fmp0,ftemp // multiply by scaled tolerance
	mls offs0,bapo0,bapo1,pconst  // point at this ntol.
	vstr fmp0,[offs0] // put the calculated tolerance per this note back.
	cmp bapo0,0
	bne gasmTunerTolLoop // if bapo0!=0 goto loop
	
	vpop {fmp0}
	pop {pconst-offs0}
	
	bx lr
	
/* float gasmTuner_freq(void); */
.global gasmTuner_freq
gasmTuner_freq:

	ldr itemp,gasmTuner_const // straight to _data
	vldr ftemp,[itemp,4] // grab freq
	
	bx lr

/* float gasmTuner_diff(void); */
.global gasmTuner_diff
gasmTuner_diff:

	ldr itemp,gasmTuner_const
	vldr ftemp,[itemp,8] // grab diff
	
	bx lr


/* uint8_t gasmTuner_octaveget(void); */
.global gasmTuner_octaveget
gasmTuner_octaveget:

	ldr itemp,gasmTuner_const
	ldr itemp,[itemp,12] // grab octave
	
	bx lr

	
	
/* uint16_t gasmTunerUpdate(void) */
	
gasmTunerInterPtr:
	.word	gasmTuner_const

.global gasmTunerUpdate
.thumb_func

gasmTunerUpdate:
	
	push {pconst-x4}
	vpush {ftemp-scope}

	ldr pconst,gasmTunerInterPtr // intention: keep pconst pointed here (gasmTuner_const) till DONE.
	ldr pdata,[pconst] // pdata = gasmTuner_data; intent: keep pdata pointed here till DONE.
	
	ldr itemp,[pconst,-20]
	ldr itemp,[itemp] // timer/counter
	mov usec,itemp // copy thereof.
	
	// Are we mid job?

	mov bapo1,0
	ldr bapo0,[pdata,20] // pointer to next portion of code to run, if any.
	str bapo1,[pdata,20] // clear that now - seems wiser to do this here than there.
	
	cbz bapo0, gasmTuner_start
	orr bapo0,bapo0,1 // turn 0n bit 0, a valid address to branch to does not have this bit set, the processor will cancel the bit after making sure it is on - if not on at this point a hard fault occurs.
	bx bapo0 // if bapo0!=0 goto bapo0; - this is a bit like a switch(arg) but there is no chance the compiler has written it as an if-then-elseif-then-elseif-then (which wouldn't be very optimal, pity I 1/10th wrote it that way anyway)

gasmTuner_start:
	
	// Are there new samples to play with ?
	
	add bapo0,pdata,-32 // point bap0 at the ISR's sample pointer offsets
	ldmia bapo0, {offs0,offs1} // offs0 and offs1 are the current sample offsets as seen in the ISR
	cmp offs0,offs1
	bne gasmTuner_st0 // if these are not equal do not check if they are not equal to our record
	add bapo0,pdata,28 // our record of those offsets.
	ldmia bapo0, {item0,item1} // item0 and item1 are our reference of those offsets. bapo0 ls stlll pointing at them
	add item0,item0,item1 // we can discard our reference for them, item0 becomes the sum of both of them
	add item1,offs0,offs1 // item1 becomes the sum of the ISR's reference figures, we keep them intact
	cmp item0,item1 // if they are equal we are going to exit, possibly via the isr.
	bne gasmTuner_start2

gasmTuner_st0:
	ldr item0, [pdata,-8] // grab the current step
	mov x4,4
	mul item0,item0,x4
	add bapo0,pdata,-24 // point bapo0 at prev_falling_cnt
	ldr item1, [bapo0,item0] // grab the previous count
	sub itemp,usec,item1 // subtract the previous counter value from the current counter value
	ldr item0, [pconst,-12] // get that const, whatever it is now (uint32_t type)
	cmp itemp,item0
	blt gasmTunerMainExit // if itemp<{INTERVAL_EQUIV_TEN_HERTZ_HERE} goto MainExit // prev blt - debugging

	ldr itemp,[pdata,0] // previous answer to itemp

	vpop {ftemp-scope}
	pop {pconst-x4} // exiting without leveling the stack would be dumb.
	
	b gasmTunerISR // use the ISR to put a timed out sample into the sample buffer, next time we get called we will run through into gasmTunerRunMain because a new sample will be there for sure...

gasmTuner_start2:
	
	stmia bapo0, {offs0,offs1} // replace our record with the record from ISR, bapo0 is freed

	add bapo0,pdata,-160 // point at start of ISR samples
	add bapo1,pdata,192 // point at start of main samples
	mov item2,15 // count of samples per section -1
	add item1,bapo1,128 // point at accums
	vsub.f32 ftt0,ftt0,ftt0
	vsub.f32 ftt1,ftt1,ftt1 // rather hoping I just zeroed both of these.
	vsub.f32 ftt3,ftt3,ftt3
	vsub.f32 fac1,fac1,fac1 // and those, for highest value sample per direction
	vldr ftt2,[pconst,-8] // big number
	vmov fac0,ftt2 // these are lowest value sample per direction
	mov x4,4
	vmov.f32 fdiv,2.0
	mla offs0,offs1,x4,bapo0 // point at oldest sample in buffers
	vldr fmp0,[offs0]
	vldr fmp1,[offs0,64]
	vadd.f32 ftt0,ftt0,fmp0
	vadd.f32 ftt1,ftt1,fmp1
	
	vcmp.f32 fmp0,ftt2
	vmrs apsr_nzcv, fpscr
	IT LT
	vmovlt ftt2,fmp0
	vcmp.f32 fmp0,ftt3
	vmrs apsr_nzcv, fpscr
	IT GT
	vmovgt ftt3,fmp0
	vcmp.f32 fmp1,fac0
	vmrs apsr_nzcv, fpscr
	IT LT
	vmovlt fac0,fmp1
	vcmp.f32 fmp1,fac1
	vmrs apsr_nzcv, fpscr
	IT GT
	vmovgt fac1,fmp1
	
	vstr fmp1,[bapo1,64]
	vstmia bapo1!,{fmp0}
gasmTuner_s2loop0:
	vmov fmp2,fmp0
	vmov fmp3,fmp1
	add offs1,offs1,1 // advancing this index
	and offs1,offs1,15 // keeping in bounds
	mla offs0,offs1,x4,bapo0
	vldr fmp0,[offs0]
	vldr fmp1,[offs0,64]
	vadd.f32 ftt0,ftt0,fmp0
	vadd.f32 ftt1,ftt1,fmp1
	vadd.f32 fmp2,fmp2,fmp0
	vadd.f32 fmp3,fmp3,fmp1
	vdiv.f32 fmp2,fmp2,fdiv
	vdiv.f32 fmp3,fmp3,fdiv
	
	vcmp.f32 fmp0,ftt2
	vmrs apsr_nzcv, fpscr
	IT LT
	vmovlt ftt2,fmp0
	vcmp.f32 fmp0,ftt3
	vmrs apsr_nzcv, fpscr
	IT GT
	vmovgt ftt3,fmp0
	vcmp.f32 fmp1,fac0
	vmrs apsr_nzcv, fpscr
	IT LT
	vmovlt fac0,fmp1
	vcmp.f32 fmp1,fac1
	vmrs apsr_nzcv, fpscr
	IT GT
	vmovgt fac1,fmp1
	
	vstr fmp1,[bapo1,64]
	vstmia bapo1!,{fmp0}
	vstr fmp3,[item1,64]
	vstmia item1!,{fmp2}
	sub item2,item2,1
	cmp item2,0
	bne gasmTuner_s2loop0

	vmov.f32 fmp0,16.0
	vdiv.f32 ftt0,ftt0,fmp0 // @ ftt0, falling average
	vdiv.f32 ftt1,ftt1,fmp0 // @ ftt1, rising average
	vstr ftt0,[item1] // last position in accums
	vstr ftt1,[item1,64] // last position in accums
	
	vadd.f32 ftt2,ftt2,ftt3
	vadd.f32 ftt3,fac0,fac1
	vdiv.f32 ftt2,ftt2,fdiv // @ ftt2, average of lowest+highest value sample from falling
	vdiv.f32 ftt3,ftt3,fdiv // @ ftt3, average of lowest+highest value sample from rising

	vstr ftt0,[pdata,64]
	vstr ftt1,[pdata,68]
	vstr ftt2,[pdata,72]
	vstr ftt3,[pdata,76] // stored for calling back in a tick, trying not to be a processor hog :)
	
	ldr itemp,[pconst,4]
	str itemp,[pdata,20]
	
	b gasmTunerMainExit
	
gasmTuner_start3:
	
	vmov.f32 fdiv,2.0
	vldr scope,[pdata,36]
	vldr ftt0,[pdata,64]
	vldr ftt1,[pdata,68]
	vldr ftt2,[pdata,72]
	vldr ftt3,[pdata,76]
	
	mov itemp,0
	vmov.f32 fmp2,2.0 /************************************ tighter scope ************************************/
//	vdiv.f32 fmp1,scope,fmp2 // divide for tighter scope here, fmp1 is carrying scope
	vmov fmp1,scope // never mind tighter scope, either scope is good or it is not.
	
	vmul.f32 fmp2,ftt0,fmp1 // fmp2 carrying scope for falling
	vmul.f32 fmp3,ftt1,fmp1 // fmp3 carrying scope for rising
	vsub.f32 ftemp,ftemp,ftemp // lol, zero is annoying - assember tells me it is junk, funny rules...
	
	vsub.f32 fmp0,ftt0,ftt2 // difference falling
	vabs.f32 fmp0,fmp0 // abs(difference)
	
	vcmp.f32 fmp2,fmp0
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_s3skip0
	
	vadd.f32 ftemp,ftemp,ftt0
	orr itemp,1
	
gasmTuner_s3skip0:
	vsub.f32 fmp0,ftt1,ftt3 // difference rising
	vabs.f32 fmp0,fmp0 // abs(difference)
	
	vcmp.f32 fmp3,fmp0
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_s3skip2
	
	vadd.f32 ftemp,ftemp,ftt1
	cbz itemp,gasmTuner_s3skip1
	
	vdiv.f32 ftemp,ftemp,fdiv // divide by two if falling average was also accepted.
gasmTuner_s3skip1:
	orr itemp,2
gasmTuner_s3skip2:
	cbz itemp,gasmTuner_s3skip3 // OK, next method then.

	str itemp,[pdata,84]
	mov itemp,1 															/**************** 1,#,0 */
	str itemp,[pdata,80]
	mov itemp,0
	str itemp,[pdata,88] // debugging, tell me the source of this detection.
	
	vstr ftemp,[pdata,96]
	ldr itemp,[pconst,8] // gasmTuner_finalAverage, expects something to play with in pdata,96; mid-scratch0 or so.
	str itemp,[pdata,20] // next stop.....
	b gasmTunerMainExit
gasmTuner_s3skip3:

	// it is tempting to write a copy of above which tries in groups of 8, due rotisserie style of input buffer.
	
	ldr itemp,[pconst,20]
	str itemp,[pdata,20] // next stop, sub-accumulations
	b gasmTunerMainExit
	

gasmTuner_subAccums:
	vldr scope,[pdata,36] // initial scope
//	vmov.f32 fdiv,2.0
//	vdiv.f32 scope,scope,fdiv // tighter scope.
	vmov.f32 fdiv,4.0
	mov offs0,0
	
	add bapo0,pdata,192 // start of all samples

	add bapo1,pdata,448 // start of sub-accums
	mov item2,bapo1 // also pointer to end of input buffer
	mov item1,0 // count of sub-accums accepted

	add item2,pdata,448 // point at end of input buffer again
	vldr ftemp,[pdata,108] // running_average
	vldmia bapo0!,{fmp2}
	vldmia bapo0!,{fmp1}
	vldmia bapo0!,{fmp0}
gasmTuner_sub6: // wrote it last and realised it should go first.
	vmov fmp3,fmp2
	vmov fmp2,fmp1
	vmov fmp1,fmp0
	vldmia bapo0!,{fmp0}
	vadd.f32 ftt0,fmp0,fmp1
	vadd.f32 ftt0,ftt0,fmp2
	vadd.f32 ftt0,ftt0,fmp3
	vdiv.f32 ftt0,ftt0,fdiv // an average
	vmul.f32 ftt2,ftt0,scope // a tailor made scope for them
	
	
	vsub.f32 ftt1,ftemp,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt1,ftt2
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub6_add

	vsub.f32 ftt1,fmp0,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub7
	
	vsub.f32 ftt1,fmp1,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub7
	
	vsub.f32 ftt1,fmp2,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub7
	
	vsub.f32 ftt1,fmp3,ftt0 // all four!
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub7
	
gasmTuner_sub6_add: // four like this is (more highly refined) equiv eight to the previous version of tuner and it would have called snap already.
	and itemp,offs0,7 // we'll only do it if it is the first four samples of one of our four groups of samples.
	cbnz itemp,gasmTuner_sub6_not_snap

	vstr ftt0,[pdata,96] // expected, average of fairly high quality.

	mov offs1,13 															/**************** 13,0,# */
	str offs1,[pdata,80]
	str itemp,[pdata,84]
	str offs0,[pdata,88] // debugging, last 5 lines - where did detection come from?
	ldr item2,[pconst,8]
	str item2,[pdata,20]
	b gasmTunerMainExit
	

gasmTuner_sub6_not_snap: // don't go snap at them more than once over this group of four, but the old tuner would call snap at this point and was seldom wrong, just wavery.
	add offs0,offs0,1

	vstmia bapo1!,{ftt0} // store average which was in tightish tolerance to source samples
	add item1,item1,1 // bump up our counter
	
	mov itemp,64
	cmp item1,itemp
	beq gasmTuner_sub_pageant
	
gasmTuner_sub7:
	cmp bapo0,item2
	blt gasmTuner_sub6

	str item1,[pdata,64] // current count of sub-accums
	str bapo1,[pdata,68] // current dest pointer
	ldr itemp,[pconst,24] // gasmTuner_sub2
	str itemp,[pdata,20] // call me back, I ain't done.
	b gasmTunerMainExit
	
gasmTuner_sub2:
	vldr scope,[pdata,36] // initial scope
//	vmov.f32 fdiv,4.0
//	vdiv.f32 scope,scope,fdiv // tighter scope.
	vmov.f32 fdiv,3.0
	
	add bapo0,pdata,192 // start of all samples
	ldr item1,[pdata,64] // restore count
	ldr bapo1,[pdata,68] // restore dest
	add item2,pdata,448 // point at end of input buffer again
	vldr ftemp,[pdata,108] // running_average
	vldmia bapo0!,{fmp1}
	vldmia bapo0!,{fmp0}
gasmTuner_sub3:
	vmov fmp2,fmp1
	vmov fmp1,fmp0
	vldmia bapo0!,{fmp0}
	vadd.f32 ftt0,fmp0,fmp1
	vadd.f32 ftt0,ftt0,fmp2
	vdiv.f32 ftt0,ftt0,fdiv // an average
	vmul.f32 ftt2,ftt0,scope // a tailor made scope for them
	
	vsub.f32 ftt1,ftemp,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt1,ftt2
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub3_add

	vsub.f32 ftt1,fmp0,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub4
	
	vsub.f32 ftt1,fmp1,ftt0
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub4
	
	vsub.f32 ftt1,fmp2,ftt0 // must be within scope to all three.
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub4
	
gasmTuner_sub3_add:
	vstmia bapo1!,{ftt0} // store average which was in tightish tolerance to source samples
	add item1,item1,1 // bump up our counter
	
	mov itemp,64
	cmp item1,itemp
	beq gasmTuner_sub_pageant
	
gasmTuner_sub4:
	cmp bapo0,item2
	blt gasmTuner_sub3

	str item1,[pdata,64] // current count of sub-accums
	str bapo1,[pdata,68] // current dest pointer
	ldr itemp,[pconst,36] // pointing at gasmTuner_sub5
	str itemp,[pdata,20]
	b gasmTunerMainExit

gasmTuner_sub5:
	vldr scope,[pdata,36] // initial scope
//	vmov.f32 fdiv,4.0
//	vdiv.f32 scope,scope,fdiv // tighter scope.
	vmov.f32 fdiv,2.0
	add bapo0,pdata,192 // start of all samples
	ldr item1,[pdata,64] // restore count
	ldr bapo1,[pdata,68] // restore dest
	vldr ftemp,[pdata,108] // running average should be here, it is an item to be compared to.
gasmTuner_sub0:	// this was first, was 2,3,4, now 4,3,2 - higher qualified first!
	vldmia bapo0!,{fmp0,fmp1}
	vadd.f32 ftt0,fmp0,fmp1
	vdiv.f32 ftt0,ftt0,fdiv // an average
	vmul.f32 ftt2,ftt0,scope // a tailor made scope for them
	vsub.f32 ftt1,ftt0,ftemp // compare to running average first
	vabs.f32 ftt1,ftt1
	vcmp.f32 ftt1,ftt2
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub0_add

	vsub.f32 ftt1,fmp0,ftt0
	vabs.f32 ftt1,ftt1 // a difference
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub1
	
	vsub.f32 ftt1,fmp1,ftt0
	vabs.f32 ftt1,ftt1 // a difference
	vcmp.f32 ftt2,ftt1
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_sub1
	
gasmTuner_sub0_add:
	vstmia bapo1!,{ftt0} // store average which was in tightish tolerance to source samples or running_average
	add item1,item1,1 // bump up our counter
	
	mov itemp,63
	cmp item1,itemp
	blt gasmTuner_sub1
	
gasmTuner_sub_pageant:
	add bapo0,pdata,448
	str bapo0,[pdata,80] // current sample being compared to others
	str bapo0,[pdata,72] // reset pointer value
	str bapo1,[pdata,76] // end pointer value
	vstr scope,[pdata,64] // this should be the right value for scope, I have seriously fucked up if not.
	vdiv.f32 scope,scope,fdiv
	vstr scope,[pdata,68] // now scope/8
	
	vsub.f32 ftemp,ftemp,ftemp
	mov itemp,0
	vstr ftemp,[pdata,116]
	vstr ftemp,[pdata,124]
	str itemp,[pdata,112]
	str itemp,[pdata,120]
	ldr itemp,[pconst,12] // gasmTuner_pageant
	str itemp,[pdata,20]
	ldr itemp,[pconst,28] // gasmTuner_sub_result
	str itemp,[pdata,24]
	b gasmTunerMainExit
	
gasmTuner_sub1:
	cmp bapo0,item2
	blt gasmTuner_sub0

	mov itemp,34
	cmp itemp,item1
	blt gasmTuner_sub_pageant // enough to work with.
	
	ldr itemp,[pconst,32] // pointer for lastChance
	str itemp,[pdata,20]
	b gasmTunerMainExit

gasmTuner_sub_result:
	
	vmov.f32 fdiv,2.0
	
	ldr item2,[pdata,120] // count tight
	ldr item0,[pdata,44] // minMatch
	cmp item2,item0
	blt gasmTuner_sub_res0

	mov item0,2 															/**************** 2,#,0 */
	str item0,[pdata,80]
	str item2,[pdata,84]
	mov item0,0
	str item0,[pdata,88] // debugging, last 5 line - where did detection come from?
	
	vldr ftemp,[pdata,124]
	vstr ftemp,[pdata,96] 
	b gasmTuner_res_exit
	
gasmTuner_sub_res0:
	ldr item2,[pdata,112] // count loose
	ldr item0,[pdata,44] // minMatch
	cmp item2,item0
	blt gasmTuner_lastChance

	mov item0,3 															/**************** 3,0,# */
	str item0,[pdata,80]
	str item2,[pdata,88]
	mov item0,0
	str item0,[pdata,84] // debugging, last 5 line - where did detection come from?
	
	vldr ftemp,[pdata,116]
	vstr ftemp,[pdata,96] 
	b gasmTuner_res_exit


	
gasmTuner_lastChance:
	
	add bapo0,pdata,192
	vmov.f32 fdiv,2.0
	vldr scope,[pdata,36]
	
	str bapo0,[pdata,80] // current sample being compared to others
	str bapo0,[pdata,72] // reset pointer value
	add bapo0,bapo0,252 // end pointer value
	str bapo0,[pdata,76]
	vldr scope,[pdata,36]
	vstr scope,[pdata,64]
	vdiv.f32 scope,scope,fdiv
	vstr scope,[pdata,68]
	
	vsub.f32 ftemp,ftemp,ftemp
	mov itemp,0
	vstr ftemp,[pdata,116] // loose average
	vstr ftemp,[pdata,124] // tight average
	str itemp,[pdata,112] // loose counter
	str itemp,[pdata,120] // tight count
	str itemp,[pdata,92] // counter of how many times we have reduced scope.
	ldr itemp,[pconst,12]
	str itemp,[pdata,20] // next stop, pageant.
	ldr itemp,[pconst,16]
	str itemp,[pdata,24] // stop after that, resulter
	b gasmTunerMainExit

gasmTuner_resulter:	
	vmov.f32 fdiv,2.0
	ldr itemp,[pdata,92]
	add item1,itemp,1
	str item1,[pdata,92] // increment our counter in memory, itemp is counter in hand.
	
/*
	ldr item2,[pdata,120] // count tight
	ldr item0,[pdata,40] // maxMatch
	cmp item2,item0
	blt gasmTuner_res0
	
	// more than maxMatch results on tight scope is enough to imagine we could go for tighter scope
	vldr ftemp,[pdata,124]
	vstr ftemp,[pdata,96] // this will be our fallback if reduction of scope hurts our results too much
	str item2,[pdata,100] // count of fallback items
	
// gasmTuner_resa:
	vldr ftemp,[pdata,68]
	vdiv.f32 ftemp,ftemp,fdiv
	vstr ftemp,[pdata,64]
	vdiv.f32 ftemp,ftemp,fdiv
	vstr ftemp,[pdata,68]
	
	vsub.f32 ftemp,ftemp,ftemp
	mov item2,0
	vstr ftemp,[pdata,116]
	vstr ftemp,[pdata,124]
	str item2,[pdata,112]
	str item2,[pdata,120]
	ldr item2,[pconst,12]
	str item2,[pdata,20] // next stop, pageant.
	ldr item2,[pconst,16]
	str item2,[pdata,24] // stop after that, resulter
	b gasmTunerMainExit

gasmTuner_res0:
*/	ldr item0,[pdata,44] // minMatch
/*	cmp item2,item0
	blt gasmTuner_res1

	mov item1,4 															/ **************** 4,#,0 * /
	str item1,[pdata,80]
	str item2,[pdata,84]
	str itemp,[pdata,88] // debugging, last 5 lines - where did detection come from?

	vldr ftemp,[pdata,124]
	vstr ftemp,[pdata,96]
	b gasmTuner_res_exit

gasmTuner_res1:
*/
	ldr item2,[pdata,112] // count on loose_scope
	cmp item0,item2
	blt gasmTuner_res2
	
//	cbz itemp,gasmTuner_markNoDetection // first pass? pffft, come back less scrambled please.
	b gasmTuner_markNoDetection
	
/*	mov offs0,6 															/ **************** 6,#,0 * /
	str offs0,[pdata,80]
	ldr item2,[pdata,100] // the count we stored in an earlier step
	str item2,[pdata,84]
	str itemp,[pdata,88] // debugging, last 5 lines - where did detection come from?
	b gasmTuner_res_exit
*/
gasmTuner_res2:	
	vldr ftemp,[pdata,116]
	vstr ftemp,[pdata,96]

	mov offs0,5 															/**************** 5,0,# */
	str offs0,[pdata,80]
	str item2,[pdata,88]
//	mov offs0,0
	str itemp,[pdata,84] // debugging, last 5 lines - where did detection come from?
	
	
gasmTuner_res_exit: // having played my cards right, [pdata,96] has either just been written or contains a fallback value from an earlier step.
	ldr item2,[pconst,8]
	str item2,[pdata,20]
	b gasmTunerMainExit





	
gasmTuner_markNoDetection:
	ldr itemp,[pdata,0] // current answer.
	mov item0,0 // make all the bits here be off
	sub item0,item0,1 // on second thoughts, turn them all on
	eor item0,item0,128 // now turn off bit 7
	and itemp,itemp,item0 // mark this as not detecting, pity I could not use the constant 65407 ffs.
	str itemp,[pdata,0]
	b gasmTunerMainExit
	
	
	
	
	
gasmTuner_pageant:
	mov x4,4
	vmov.f32 fdiv,2.0
	vldr ftt2,[pdata,64] //=scope_loose
	vldr ftt3,[pdata,68] //=scope_tight
	ldr bapo0,[pdata,72] //=reset_pointer_value
	ldr bapo1,[pdata,76] //=end_pointer_value
	ldr offs0,[pdata,80] //=current_major_pointer, should have started on the reset_pointer_value
	mov offs1,offs0 //=minor_pointer
	vldr fac0,[offs0] //=current_accumulator_loose
	vmov fac1,fac0 //=current_accumulator_tight
	vmov fmp0,fac0 //=current_comparison_value
	mov item0,1 //=current_count_loose
	mov item1,1 //=current_count_tight
gasmTuner_pag0:
	cmp offs1,bapo1
	bne gasmTuner_pag0a
	ldr offs1,[pdata,72] // reset to beginning of samples
	b gasmTuner_pag1
	
gasmTuner_pag0a:
	add offs1,offs1,x4 // shift forward
gasmTuner_pag1:
	cmp offs0,offs1
	beq gasmTuner_pag_finish // we started on the sample after 'major' and end on 'major' without comparing to self
	
	vldr fmp1,[offs1]
	vadd.f32 fmp2,fmp0,fmp1
	vmul.f32 fmp3,fmp2,ftt3 // fmp3 is allowable abs(diff) for tight
	vmul.f32 fmp2,fmp2,ftt2 // fmp2 is allowable abs(diff) for loose
	
	vsub.f32 ftt0,fmp0,fmp1
	vabs.f32 ftt0,ftt0 // absolute difference between these samples
	
	vcmp.f32 fmp2,ftt0
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_pag2
	
	vadd.f32 fac0,fac0,fmp1 // add to loose accumulator
	add item0,item0,1 // increment loose counter
gasmTuner_pag2:
	vcmp.f32 fmp3,ftt0
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_pag0
	
	vadd.f32 fac1,fac1,fmp1 // add to tight accumulator
	add item1,item1,1 // increment tight counter
	b gasmTuner_pag0 // continue on merry way
	
gasmTuner_pag_finish:
	ldr item2,[pdata,112] // best count loose
	cmp item0,item2
	blt gasmTuner_pag3
	
	vstr fac0,[pdata,116]
	str item0,[pdata,112] // better count loose
gasmTuner_pag3:
	ldr item2,[pdata,120] // best count tight
	cmp item1,item2
	blt gasmTuner_pag4
	
	vstr fac1,[pdata,124]
	str item1,[pdata,120] // better count tight
gasmTuner_pag4:
	cmp offs0,bapo1
	beq gasmTuner_pag5
	
	add offs0,offs0,4
	str offs0,[pdata,80]
	ldr itemp,[pconst,12]
	str itemp,[pdata,20] // call me back, I am not finished.
	b gasmTunerMainExit // but I am taking a break :)
	
gasmTuner_pag5:
	
	vldr ftemp,[pdata,116]
	ldr itemp,[pdata,112]
	vmov fmp0,itemp
	vcvt.f32.u32 fmp0,fmp0
	vdiv.f32 ftemp,ftemp,fmp0
	vstr ftemp,[pdata,116]
	
	vldr ftemp,[pdata,124]
	ldr itemp,[pdata,120]
	vmov fmp0,itemp
	vcvt.f32.u32 fmp0,fmp0
	vdiv.f32 ftemp,ftemp,fmp0
	vstr ftemp,[pdata,124]
	
	ldr itemp,[pdata,24] // this had better be zero or a good place to send the PC
	str itemp,[pdata,20]
	mov itemp,0
	str itemp,[pdata,24]
	b gasmTunerMainExit
	
	

	











	








	
gasmTuner_finalAverage:

	mov x4,4
	vmov.f32 fdiv,2.0
	vldr scope,[pdata,36]
//	vdiv.f32 scope,scope,fdiv // tighten scope a bit :)
	vldr fac0,[pdata,96] // grab back our detected frequency, all routines that pass to here place the value here in scratch0
	vmul.f32 ftt1,fac0,scope // as usual scope is a construct of our current value multiplied by whatever scope may be, in ftt1 atm.
	
	ldr offs0,[pdata,52] // grab averages offset
	add bapo1,pdata,56 // point at averages
	
	vldmia bapo1,{fmp0,fmp1} // grab previous averages
	
	mla offs1,offs0,x4,bapo1 // point at the average we wish to replace
	vstr fac0,[offs1] // put our new average in there

	add offs0,1
	and offs0,1 // there are only two prior averages we are tracking
	str offs0,[pdata,52] // put back pointer to next average to overwrite next time.

	vadd.f32 ftt0,fmp0,fmp1 // I may want this value of ftt0 if I make it past a blt coming soon.
	vdiv.f32 ftemp,ftt0,fdiv // ftt0 is sum of previous averages, ftemp just became average of those
	vsub.f32 ftemp,ftemp,fac0 // ftemp is the difference between previous averages and this one
	vabs.f32 ftemp,ftemp // ftemp is the abs(difference)
	vcmp.f32 ftt1,ftemp // ftt1 should be (0.001*fac0)+5, scope, if ftt1<ftemp then difference is too large.
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_markNoDetection // lol, pity, I really thought we had something going there...
	
	vadd.f32 ftt0,ftt0,fac0 // you know I love averaging shit, right?
	vmov.f32 fac0,3.0
	vdiv.f32 ftt0,ftt0,fac0 // it is almost 99.9% gauranteed that ftt0 is the dominant period here.
	vstr ftemp,[pdata,108] // stash this as a running average.
	vldr ftemp,[pconst,-8] // const timer/counter rate per second
	vdiv.f32 ftemp,ftemp,ftt0 // and so 99.9% sure of tone frequency here.
	
	
	vmov.f32 ftt0,15.0
	vcmp.f32 ftemp,ftt0
	vmrs apsr_nzcv, fpscr
	blt gasmTuner_markNoDetection // Nope, that far below C0 can take a running jump, we are sure of this frequency tho.
	
	@ftemp: detection frequency // the following section of code determines nearest note and difference
	
	ldr itemp,[pconst,-4] // grab the default response
	orr itemp,128 // detecting!
	str itemp,[pdata,0] // put that response in the output slot.
	str usec,[pdata,16] // put the time in its slot
	vstr ftemp,[pdata,4] // store our detected frequency
	
	vmov fac0,ftemp // copy our frequency
	mov item2,0 // prepare an octave count
	vldr fmp0,[pconst,-16] // grab octava
	vldr fmp2,[pdata,48] // note_tolerance
gtrmloophere3:
	vcmp.f32 fac0,fmp0
	vmrs apsr_nzcv, fpscr
	blt gtrmskiphere4
	
	vdiv.f32 fac0,fac0,fdiv
	vdiv.f32 fmp2,fmp2,fdiv
	add item2,1
	b gtrmloophere3
	
gtrmskiphere4:
	// it is another popularity contest, this time we are picking the note...
	
	add bapo0,pconst,48 // point bapo0 at the notes
	@fmp0: octava, we will use fmp0 as the 'current best difference'
	@item2: The current octave, maybe cleverer to store immediately before I accidentally reassign item2
	
	str item2,[pdata,12] // store the current octave
	
	mov item1,12 // the likely note
	mov item0,12 // the note we are about to check +1
gtrmloophere4:	
	sub item0,1
	mla offs0,item0,x4,bapo0 // point at a note value
	vldr fmp1,[offs0] // get the note-value
	
	vsub.f32 ftt0,fmp1,fac0 //,fmp1 // get the difference into ftt0
	vabs.f32 ftt1,ftt0 // get abs(difference) into ftt1

	vcmp.f32 fmp0,ftt1 // compare to previous smallest difference
	vmrs apsr_nzcv, fpscr
	blt gtrmskiphere5 // previous smallest is smaller
	
	mov item1,item0
	vmov fmp0,ftt1 // abs difference here
	vmov ftemp,ftt0 // actual difference here, important!
	
gtrmskiphere5:
	cmp item0,0
	bne gtrmloophere4
	
	// notenum: item1, actual difference: ftemp
	
	// we wish to multiply the actual difference by the ratio for the note it came from, this gives the display routine a proportionate amount to play with for each note.
	
	and itemp,itemp,65280 // remove note record
	orr itemp,itemp,item1 // place this note record.
	eor itemp,itemp,128 // re-mark detecting, allowable immediate constants here are a pita.
	
	add bapo0,pconst,96 // point at ratios
	mla offs0,item1,x4,bapo0 // point at the ratio I want
	
	vldr fmp1,[offs0] // grab that ratio
	
	vmul.f32 fmp1,ftemp,fmp1 // multiply the difference by the ratio
	
	vstr fmp1, [pdata,8] // place this value there
	
	// ok, guess we had better set LEDs and things
	
	add bapo0,pdata,128 // point at nTols
	mla offs0,item1,x4,bapo0 // point at the nTol I want
	
//	vldr fmp1,[offs0] // get the nTol into fmp1, fmp0 has abs(diff) and is what to compare to for 'tuned'
	
	vcmp.f32 fmp2,fmp0
	vmrs apsr_nzcv, fpscr 
	blt gtrmskiphere6 // it is not less than the tolerance amount
	
	// tuned, we need to stow itemp at pdata+0 because it is loaded from there very next thing.
	eor itemp,512 // toggle this bit to 0 to indicate tuned.
	str itemp,[pdata,0] // pop the answer away
	b gasmTunerActualExit

gtrmskiphere6:

//	vmov.f32 ftt0,0
	vsub.f32 ftt0,ftt0,ftt0 // make this equal 0 because assembler is a bit of a fuckwit.
	vcmp.f32 ftemp,ftt0
	vmrs apsr_nzcv, fpscr 
	bgt gtrmskiphere7 // it is less than 0
	
	eor itemp,256 // toggle this bit to 0 to indicate tight
	b gtrmskiphere8

gtrmskiphere7:

	eor itemp,1024 // toggle this bit to 0 to indicate loose
	
gtrmskiphere8:
	
	str itemp,[pdata,0] // pop the answer away

	vmov.f32 ftt0,2.0
	vmul.f32 fmp2,fmp2,ftt0
	
	vcmp.f32 fmp2,fmp0
	vmrs apsr_nzcv, fpscr 
	blt gasmTunerActualExit
	
	// very close to tuned, turn on tuned led to show close...
	eor itemp,512 // toggle this bit to 0 to indicate tuned.

	str itemp,[pdata,0] // pop the answer away, I can see the next line...
	
	
	
	
gasmTunerActualExit:




	ldr itemp,[pdata,0] // previous answer to itemp, if I wrote it back there I should have stored it shortly after modifying it.
	
	vpop {ftemp-scope}
	pop {pconst-x4}

	bx lr

gasmTunerMainExit:
	ldr bapo0,[pdata,16] // last 'hit'
	sub itemp,usec,bapo0 // interval
	ldr bapo1,[pconst,-12] // timer/~5
	mov item0,5
	mul bapo1,bapo1,item0 // ~ 1 second
	cmp itemp,bapo1
	blt gasmTunerActualExit
	
	ldr itemp,[pconst,-4] // grab the default response
	str itemp,[pdata,0] // put that response in the output slot.
	str usec,[pdata,16]
	
	vsub.f32 ftemp,ftemp,ftemp // lol zero.
	vstr ftemp,[pdata,4] // store 0 as our detected frequency, if execution is here pdata,0 already contains appropriate response.
	vstr ftemp,[pdata,8] // store 0 as our detected difference as well.
	mov itemp,0
	str itemp,[pdata,12] // store 0 as our detected octave now too.

	b gasmTunerActualExit

