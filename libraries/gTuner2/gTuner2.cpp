

#include <inttypes.h>
#include "gTuner2.h"

#ifdef __cplusplus
extern "C" {
#endif

void gasmTunerISR(void);
void gasmTunerTolerance(float newTolerance);
uint8_t* gasmTuner_noteName(uint8_t noteNum);
float gasmTuner_freq(void);
float gasmTuner_diff(void);
uint8_t gasmTuner_octaveget(void);
uint32_t* gasmTuner_getBit(uint32_t offset);
uint32_t* gasmTuner_getPtr(int32_t offset);
uint16_t gasmTunerUpdate(void);
uint16_t gasmTuner_result(float freq);
void gasmTuner_initCounts(void);

#ifdef __cplusplus
}
#endif


void gTuner2::begin(uint8_t pin_num)
{

	pinMode(pin_num,INPUT);
	_pin_num=pin_num;
	tolerance(0.05);

}

// void ftm1_isr(void) { gasmFtm1_isr(); }

void gTuner2::startISR(void)
{
	gasmTuner_initCounts();
	
	attachInterrupt(_pin_num,gasmTunerISR,CHANGE);
	NVIC_SET_PRIORITY(IRQ_PORTC, 64); // bit of a shit way to do it, no gaurantee they don't use a pin from NOT port c)
}	

uint16_t gTuner2::read(void) { return gasmTunerUpdate(); }

void gTuner2::tolerance(float newTol) { gasmTunerTolerance(newTol); }
uint8_t* gTuner2::noteName(uint8_t noteNum) { return gasmTuner_noteName(noteNum); }

float gTuner2::freq(void) { return gasmTuner_freq(); }
float gTuner2::diff(void) { return gasmTuner_diff(); }
uint8_t gTuner2::octave(void) {return gasmTuner_octaveget(); }

uint16_t gTuner2::result(float freq) {return gasmTuner_result(freq); }

uint32_t* gTuner2::ptrTest(int32_t ptroff) { return gasmTuner_getPtr(ptroff); }
uint32_t* gTuner2::bitTest(int32_t ptroff) { return gasmTuner_getBit(ptroff); }

